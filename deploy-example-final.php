<?php
namespace Deployer;

require 'recipe/common.php';

set('allow_anonymous_stats', false);

set('application', function() {
	return getenv('CI_PROJECT_NAME');
});

set('user', function(){
	return getenv('GITLAB_USER_NAME');
});

set('repository', function() {
	return getenv('CI_REPOSITORY_URL');
});

set('deploy_path', function() {
	$basePath = getenv('DEPLOYER_BASE_PATH');
	$projectName = getenv('CI_PROJECT_NAME');
	$environment = get('stage');

	return "$basePath/$environment/$projectName";
});

set('shared_dirs', ['log', 'www/upload']);
set('shared_files', ['config/config.local.neon']);
set('writable_dirs', ['log', 'temp', 'www/assets/css', 'www/assets/js/apploud', 'www/assets/js/libs']);
set('clear_paths', ['temp/cache', 'temp/proxies']);
set('copy_dirs', ['node_modules', 'vendor', 'www/assets/css', 'www/assets/js']);

host('dev')
	->stage('dev')
	->hostname(getenv('DEV_HOST'))
	->user('deployer')
	->forwardAgent(false)
	->addSshOption('StrictHostKeyChecking', 'no');

host('client')
	->stage('client')
	->hostname(getenv('CLIENT_HOST'))
	->user('deployer')
	->forwardAgent(false)
	->addSshOption('StrictHostKeyChecking', 'no');

host('production')
	->stage('production')
	->hostname(getenv('PRODUCTION_HOST'))
	->user('deployer')
	->forwardAgent(false)
	->addSshOption('StrictHostKeyChecking', 'no');

task('deploy:db', 'APPLOUD_SERVER_ENVIRONMENT={{stage}} bin/console migrations:migrate --no-interaction --allow-no-migration');

task('deploy:build', '
    composer install
    composer dump-autoload
    npm install
    npm run build
');

task('deploy', [
	'deploy:info',
	'deploy:prepare',
	'deploy:lock',
	'deploy:release',
	'deploy:update_code',
	'deploy:shared',
	'deploy:writable',
	'deploy:copy_dirs',
	'deploy:build',
	'deploy:clear_paths',
	'deploy:db',
	'deploy:symlink',
	'deploy:unlock',
	'cleanup',
	'success'
]);

after('deploy:failed', 'deploy:unlock');
